def checkPrime(n):
    count = 0
    for i in range(2,(n//2)+1):
        if(n%i==0):
            count+=1
    if count>0:
        return False
    else:
        return True
N=int(input())
s = 0
for i in range(2,N+1):
    if checkPrime(i):
        print(i)
        s+=i
print("Sum is: "+ str(s))
