def binary(arr,low,high,x):
    if(high>=low):
        mid = (high+low)//2
        if arr[mid] == x:
            return mid
        elif arr[mid]>x:
            return binary(arr,low,mid-1,x)
        else:
            return binary(arr,mid+1,high,x)
    else:
        return -1
n = int(input("Enter the number of elements: "))
l=[]
for _ in range(n):
    l.append(int(input()))
m=int(input("Enter the search value: "))
result = binary(l,0,n,m)
if result == -1:
    print("")
else:
    print(str(m)+" found at location "+str(result+1))

              
